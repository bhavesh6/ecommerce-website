export class Product {
    id: number;
    name: string;
    category: string;
    brand: string;
    oldprice: number;
    price: number;
    discount: string;
    color: string;
    gender: string;
    image: string;
    size: string;
}

export class Category {
    id: number;
    name: string;
    value: string;
}

export class Size {
    id: number;
    name: string;
    value: string;
}

export class Brand {
    id: number;
    name: string;
    value: string;
}

export class Gender {
    id: number;
    name: string;
    value: string;
}
export class Color {
    id: number;
    name: string;
    value: string;
}
