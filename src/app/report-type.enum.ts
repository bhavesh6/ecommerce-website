export enum ReportType {
    COURSE = 'Course',
    WORKFLOW = 'Workflow'
}
