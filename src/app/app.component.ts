import { Component } from '@angular/core';
import { ReportType } from './report-type.enum';
import { Color, Size } from './course.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  colorList: Color[];
  sizeList: Size[];
  selectedColor: string;
  selectedSize: string;
  constructor() {
    this.colorList = [{
      id: 1,
      name: 'Black',
      value: 'Black'
    }, {
      id: 2,
      name: 'White',
      value: 'White'
    }, {
      id: 3,
      name: 'Blue',
      value: 'Blue'
    }];

    this.sizeList = [{
      id: 1,
      name: 'Small',
      value: 'S'
    }, {
      id: 2,
      name: 'Medium',
      value: 'L'
    }, {
      id: 3,
      name: 'Large',
      value: 'L'
    }];

    this.selectedColor = this.colorList[0].name;
    this.selectedSize = this.sizeList[0].name;
  }
  eReportType: any = ReportType;
  title = ReportType.COURSE;
}
