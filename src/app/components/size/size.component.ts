import { Component, OnInit, Input } from '@angular/core';

import { Size } from '../../course.model';
@Component({
  selector: 'app-size',
  templateUrl: './size.component.html',
  styleUrls: ['./size.component.css']
})
export class SizeComponent implements OnInit {
  @Input() sizeList: Size[];
  @Input() selectedSize: string;

  constructor() { }

  ngOnInit() {
  }

}
