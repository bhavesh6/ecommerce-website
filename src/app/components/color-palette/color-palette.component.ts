import { Component, OnInit, Input } from '@angular/core';
import { Color } from '../../course.model';

@Component({
  selector: 'app-color-palette',
  templateUrl: './color-palette.component.html',
  styleUrls: ['./color-palette.component.css']
})
export class ColorPaletteComponent implements OnInit {
@Input() colorList: Color[];
@Input() selectedColor: string;
  constructor() { }

  ngOnInit() {
  }

}
